# About
### EXD-Server is an express Web server for:
* Sending and retrieving data for untitled music application

# Requires  
* Node 10.x

# Setup 
* Recommended OS OSX / Windows
* Install Visual Studio Code (Recommended)
    * Install ESLint plugin 
    * Install GitLens plugin
* Install Git
* Check out from Bitbucket using Git 
* Run `npm install` to install dependencies
* Run `npm start` or run config `start` in Visual Studio Code


# Commands 
* `npm start` Start Express web server.  

# Best Practices
* Do not commit to master
* Save all dependencies to packages.json when doing an NPM install
* Before doing an NPM install check if dependency is a dev dependency, if so install it as a dev dependency. 

# Project Structure  

* **src** (*Express web server*)
    * **config** (*Database configuration*)
    * **routes** (*Server API routing*)
    * **utils** (*Reusable helper classes*)