const NodeID3 = require('node-id3');
const dirTree = require('directory-tree');
const tree = dirTree('C://users/matt/desktop/music');
const files = tree && tree.children;
const databaseController = require('../controllers/databaseController');
const fs = require('fs');

parseFiles = () => {
    files.forEach(file => {
        const path = file.path

        NodeID3.read(path, function (err, tag) {
            console.log(tag)
            if (tag && tag.title) {
                const song = {
                    songTitle: tag.title ? tag.title.substring(0,20) : 'n/a',
                    songAlbum: tag.album ? tag.album.substring(0,20) : 'n/a',
                    songYear: tag.year ? tag.year : 'n/a',
                    trackNumber: tag.trackNumber ? tag.trackNumber : 'n/a',
                    songArtist: tag.artist ? tag.artist.substring(0,20) : 'n/a',
                    songLength: tag.length ? tag.length : 'n/a',
                    genre: tag.genre ? tag.genre.substring(0,20) : 'none',
                    fileName: file.name
                }

                if (song.songTitle && song.songTitle.length > 35) {
                    song.songTitle = song.songTitle.substring(0,20);
                }

                console.log(song)
                const str =  `SONGS_TAPI.insert_song (
                SONG_SEQ.NEXTVAL,
                '${song.songTitle}',
                '${song.songArtist}',
                '${song.songAlbum}',
                '${song.trackNumber}',
                '${song.genre}',
                '${song.songYear}',
                '${song.fileName}'
                '${song.songLength}',
                );
                \n`

                // databaseController.insertSong(songEntry)
                fs.appendFile('test.txt', str, function (err) {
                    if (err) {
                        return console.log(err);
                    }

                    console.log('The file was saved!');
                });
            }
            else {
                console.log('no title ' + tag)
            }
        })
    })

    console.log('done')
    databaseController.connection.close(
        function (err) {
            if (err)
                console.error(err.message)
        }
    );
}

module.exports = { parseFiles }
