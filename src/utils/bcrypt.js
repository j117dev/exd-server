const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

hashPassword = async (password) => {
    const hash = await bcrypt.hash(password, saltRounds)
    .then((hash) => {
      return hash
    })
    return hash
  }

module.exports = { hashPassword }