const { hashPassword } = require('../utils/bcrypt')
const types = require('../types/user');
const oracledb = require('oracledb');

class databaseController {
    constructor() {
        this.connection = null;
    }

    /**
    * This function will return one user
    * @param username The username of the user
    * @returns A promise with a user
    */
    getUserByUsername(username) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT *
                FROM EXD_USER WHERE exd_user.username = :username`,
                { username: username },
                (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(result.rows[0]);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will return all users
     * @returns A promise an array of users
     */
    getUsers() {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT * FROM EXD_USER`,
                (err, result) => {
                    if (err) {
                        doRelease(this.connection);
                        return reject(err);
                    }
                    resolve(result.rows);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will check whether a user exists in the database
     * @param username The username of the user
     * @returns True or False
     */
    async userExists(username) {
        const user = await this.getUserByUsername(username);

        if (user) {
            return true;
        }
        return false;
    }

    /**
     * This function will add a user to the database
     * @param user The user object to be added
     * @returns A promise with number of rows affected
     */
    async registerUser(user) {
        const exists = await this.userExists(user.username);

        if (exists) {
            return {
                error: types.USER_EXISTS
            };
        }

        const password = await hashPassword(user.password);

        return new Promise((resolve, reject) => {
            this.connection.execute(
                `INSERT INTO EXD_USER
                VALUES (
                    user_seq.NEXTVAL,
                    :firstName,
                    :lastName,
                    TO_DATE(:birthday, 'yyyy/mm/dd'),
                    :username, 
                    :password, 
                    :genrePreference, 
                    'user'
                    )`,
                {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    birthday: user.birthday,
                    username: user.username,
                    password: password,
                    genrePreference: user.genrePreference
                },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result);
                }
            );
        })
            .catch((e) => {
                console.log(e)
            })
    }

    /**
    * This function will update a user in the database
    * @param details The details of the user ot be updated
    * @returns A promise with number of rows affected
    */
    async updateUser(details) {
        const user = await this.getUserByUsername(details.username);

        if (!user) {
            return {
                error: types.USER_EXISTS
            };
        }

        return new Promise((resolve, reject) => {
            this.connection.execute(
                `UPDATE EXD_USER SET 
                        firstname = :firstName,
                        lastname = :lastName,
                        genrepreference = :genrePreference
                        WHERE username = :username
                        `,
                {
                    firstName: details.firstName || user.FIRSTNAME,
                    lastName: details.lastName || user.LASTNAME,
                    genrePreference: details.genrePreference || user.GENREPREFERENCE,
                    username: details.username
                },
                function (err, result) {
                    if (err) {
                        console.log(err);
                        return reject({ error: err });
                    }
                    resolve(result);
                }
            );
        })
            .catch((e) => {
                console.log(e)
            })
    }

    //Songs

    /**
     * This function will perform a select all on the songs table 
     * @returns A promise with rows in the songs table
     */
    getAllSongs() {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT * FROM SONGS`,
                function (err, result) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(result.rows);
                }
            );
        })
    }

    /**
     * This function will insert a new entry into the songs table 
     * @param song The song object to be inserted
     * @returns A promise with number of rows affected
     */
    async insertSong(payload) {
        return new Promise((resolve, reject) => {
                this.connection.execute(
                    `INSERT INTO SONGS VALUES (
                    SONG_SEQ.NEXTVAL,
                    :songTitle,
                    :songArtist,
                    :songAlbum,
                    :trackNumber,
                    :genre,
                    :songYear,
                    :fileName,                   
                    :songLength
                    )`,
                    {
                        songTitle: payload.song.songTitle,
                        songArtist: payload.song.songArtist,
                        songAlbum: payload.song.songAlbum,
                        trackNumber: payload.song.trackNumber,
                        genre: payload.song.genre,
                        songYear: payload.song.songYear,
                        fileName: payload.song.fileName,
                        songLength: payload.song.songLength
                    },
                    function (err, result) {
                        if (err) {
                            console.log(err);

                            return reject(err);
                        }
                        resolve(result);
                    }
                );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will delete an entry from the songs table with a given song id
     * @param songID The ID of the song
     * @returns A promise with number of rows affected
     */
    async deleteSongByID(songID) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `DELETE FROM SONGS 
                WHERE SONGID = :songID`,
                { 
                    songID: parseInt(songID) 
                },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will update an entry from the songs table by song id
     * @param song The updated song object
     * @returns A promise with number of rows affected
     */
    async updateSong(songID, song) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `UPDATE SONGS SET                 
                    SONGTITLE = :songTitle,
                    SONGARTIST = :songArtist,
                    SONGALBUM = :songAlbum,
                    TRACKNUMBER = :trackNumber,
                    GENRE =  :genre,
                    SONGYEAR = :songYear,
                    SONGLENGTH = :songLength
                    WHERE SONGID = :songID
                    `,
                {
                    songTitle: song.songTitle,
                    songArtist: song.songArtist,
                    songAlbum: song.songAlbum,
                    trackNumber: song.trackNumber,
                    genre: song.genre,
                    songYear: song.songYear,
                    songLength: song.songLength,
                    songID: parseInt(songID)
                },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will get all songs from the songs table by filter
     * @param filter The field to filter by (ie genre)
     * @param value The value that the filter should match (ie rap)
     * @returns A promise with an arrary of songs
     */
    async getSongsByFilter(filter, value) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT * FROM SONGS WHERE ${filter} = :fValue`,
                {
                    fValue: value
                },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result.rows);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will retrieve the suggestions of a given user
     * @param userID The ID of the user
     * @returns A promise with an array of suggested songs
     */
    async getSuggestions(userID) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT suggestions.suggestionid, exd_user.username, 
                suggestions.userid, songs.songid, songs.songtitle, 
                songs.songartist, songs.songalbum, songs.songyear, songs.genre, 
                songs.filename 
                FROM SONGS, SUGGESTIONS, exd_user
                WHERE suggestions.songID = songs.songID
                AND suggestions.userid = :userID
                AND exd_user.userid = :userID`,
                { userID: parseInt(userID) },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result.rows);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will generate a suggestion, and insert it into the suggestions table
     * @param genre The genre of the suggested song.
     * @param userID The ID of the user
     * @returns The suggested song that was inserted
     */
    async createSuggestion(genre, userID) {
        const suggestion = await this.generateSuggestion(genre);
        await this.insertSuggestion(`${suggestion.SONGID}`, userID);

        return suggestion;
    }

    /**
     * This function will get a suggested song from the songs table based on genre
     * @param genre The genre of the suggested song.
     * @returns A promise with an arrary of songs
     */
    async generateSuggestion(genre) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `SELECT *
                FROM SONGS
                WHERE GENRE = :genre
                ORDER BY dbms_random.value`,
                { genre: genre },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result.rows[0]);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }

    /**
     * This function will insert a suggested song into the suggestions table
     * @param songID The ID of the song
     * @param userID The ID of the user
     * @returns A promise with the number of rows affected
     */
    async insertSuggestion(songID, userID) {
        return new Promise((resolve, reject) => {
            this.connection.execute(
                `INSERT INTO SUGGESTIONS
                VALUES (
                    suggestion_seq.NEXTVAL,
                    :userID,
                    :songID)`,
                {
                    userID: parseInt(userID),
                    songID: parseInt(songID)
                },
                function (err, result) {
                    if (err) {
                        console.log(err);

                        return reject(err);
                    }
                    resolve(result);
                }
            );
        })
            .catch((e) => {
                console.log(e);
            })
    }
}


function doRelease(connection) {
    connection.close(
        function (err) {
            if (err)
                console.error(err.message)
        }
    );
}

module.exports = new databaseController()
