const express = require('express');
const app = express();
const users = require('./routes/users');
const songs = require('./routes/songs');
const DatabaseController = require('./controllers/databaseController');
const oracledb = require('oracledb');
oracledb.outFormat = oracledb.OBJECT;
oracledb.autoCommit = true;
const morgan = require('morgan');
const { parseFiles } = require('./utils/mp3_scraper');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv').config();

app.use(cookieParser());
app.options('*', cors());
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

  //routes
app.use('/users', users);
app.use('/songs', songs);
//logging
app.use(morgan('combined'));

app.listen(process.env.PORT || 8080, () => console.log(`Server listening on port 80`))

//Set up database connection
oracledb.getConnection(
    {
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        connectString: process.env.DB_HOST
    },
    (err, connection) => {
        if (err) {
            console.error(err.message);
            return;
        }
        DatabaseController.connection = connection;
    }
)