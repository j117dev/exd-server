const types = {
    USER_NOT_FOUND: 'User not found',
    INVALID_PASSWORD: 'Invalid password',
    USER_EXISTS: 'User already exists'
}

module.exports = types