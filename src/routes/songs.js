
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const DatabaseController = require('../controllers/databaseController')

router.get('/', asyncHandler(async (req, res) => {
  const songs = await DatabaseController.getAllSongs();
  
  res.json(songs);
}));

router.post('/insert', asyncHandler(async (req, res) => {
  const song = req.body.song;
  const insert = await DatabaseController.insertSong(song);
  
  res.json(insert);
}));

router.post('/delete', asyncHandler(async (req, res) => {
  const songID = req.body.songID;
  const result = await DatabaseController.deleteSongByID(songID);
  
  res.json(result);
}));

router.post('/update', asyncHandler(async (req, res) => {
  const songID = req.body.songID;
  const update = req.body.update;
  
  const result = await DatabaseController.updateSong(songID, update);
  
  res.json(result);
}));

router.post('/filter', asyncHandler(async (req, res) => {
  const filter = req.body.filter;
  const value = req.body.value;
  const songs = await DatabaseController.getSongsByFilter(filter, value);
  
  res.json(songs);
}));

router.post('/suggestion', asyncHandler(async (req, res) => {
  const genre = req.body.genre;
  const userID = req.body.userID;
  const suggestion = await DatabaseController.createSuggestion(genre, userID);
  
  res.json(suggestion);
}));

router.post('/suggestions', asyncHandler(async (req, res) => {
  const userID = req.body.userID;
  const suggestions = await DatabaseController.getSuggestions(userID);
  
  res.json(suggestions);
}));

module.exports = router