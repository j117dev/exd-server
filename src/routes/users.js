
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const DatabaseController = require('../controllers/databaseController');
const types = require('../types/user');
const bcrypt = require('bcrypt');

router.get('/', asyncHandler(async (req, res) => {
  const users = await DatabaseController.getUsers();

  res.json(users);
}));

router.post('/login', asyncHandler(async (req, res) => {
  const user = req.body.user;
  //Get user from database
  const userFromDB = await DatabaseController.getUserByUsername(user.username);

  if (userFromDB && !userFromDB.error) {
    bcrypt.compare(user.password, userFromDB.PASSWORD)
      .then(function (match) {
        if (match) {

          const userObj = {
            userName: userFromDB.USERNAME,
            firstName: userFromDB.FIRSTNAME,
            lastName: userFromDB.LASTNAME,
            userID: userFromDB.USERID,
            userRole: userFromDB.USERROLE,
            genrePreference: userFromDB.GENREPREFERENCE,
            birthday: userFromDB.BIRTHDAY
          }

          const payload = {
            user: userObj
          };

          return res.json(payload);
        }
        else {
          //If passwords do not match
          const payload = {
            error: {
              message: types.INVALID_PASSWORD
            }
          };
          return res.json(payload);
        }
      });
  }
  else {
    //If user does not exist (undefined)
    const payload = {
      error: {
        message: types.USER_NOT_FOUND
      }
    };
    return res.json(payload);
  }
}));

router.post('/register', asyncHandler(async (req, res) => {
  const newUser = req.body.user;
  const resp = await DatabaseController.registerUser(newUser);

  return res.json(resp);
}));


router.post('/editAccountDetails', asyncHandler(async (req, res) => {
  const accountDetails = req.body.details;
  const update = await DatabaseController.updateUser(accountDetails);

  if (typeof update === 'undefined') {
    return res.json(update.error)
  }

  const userFromDB = await DatabaseController.getUserByUsername(accountDetails.username)

  const userObj = {
    userName: userFromDB.USERNAME,
    firstName: userFromDB.FIRSTNAME,
    lastName: userFromDB.LASTNAME,
    userID: userFromDB.USERID,
    userRole: userFromDB.USERROLE,
    genrePreference: userFromDB.GENREPREFERENCE,
    birthday: userFromDB.BIRTHDAY
  }

  return res.json(userObj);
}));


module.exports = router